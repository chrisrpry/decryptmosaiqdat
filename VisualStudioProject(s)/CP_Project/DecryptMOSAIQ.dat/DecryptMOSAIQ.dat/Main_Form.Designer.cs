﻿namespace DecryptMOSAIQ.dat
{
    partial class Main_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_Form));
            this.textBoxSource = new System.Windows.Forms.TextBox();
            this.textBoxOutput = new System.Windows.Forms.TextBox();
            this.ButtonDecrypt = new System.Windows.Forms.Button();
            this.ButtonEncrypt = new System.Windows.Forms.Button();
            this.ButtonAuthorize = new System.Windows.Forms.Button();
            this.ButtonResetForm = new System.Windows.Forms.Button();
            this.LabelVersionAuthor = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxSource
            // 
            this.textBoxSource.AllowDrop = true;
            this.textBoxSource.Enabled = false;
            this.textBoxSource.Location = new System.Drawing.Point(12, 12);
            this.textBoxSource.Multiline = true;
            this.textBoxSource.Name = "textBoxSource";
            this.textBoxSource.Size = new System.Drawing.Size(373, 150);
            this.textBoxSource.TabIndex = 3;
            this.textBoxSource.Text = "Drag MOSAIQ.dat file, or paste contents here...";
            this.textBoxSource.DragDrop += new System.Windows.Forms.DragEventHandler(this.textBoxSource_DragDrop);
            this.textBoxSource.DragEnter += new System.Windows.Forms.DragEventHandler(this.textBoxSource_DragEnter);
            this.textBoxSource.Enter += new System.EventHandler(this.textBoxSource_Enter);
            // 
            // textBoxOutput
            // 
            this.textBoxOutput.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxOutput.Enabled = false;
            this.textBoxOutput.Location = new System.Drawing.Point(12, 168);
            this.textBoxOutput.Multiline = true;
            this.textBoxOutput.Name = "textBoxOutput";
            this.textBoxOutput.ReadOnly = true;
            this.textBoxOutput.Size = new System.Drawing.Size(373, 200);
            this.textBoxOutput.TabIndex = 4;
            // 
            // ButtonDecrypt
            // 
            this.ButtonDecrypt.Enabled = false;
            this.ButtonDecrypt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonDecrypt.Location = new System.Drawing.Point(391, 54);
            this.ButtonDecrypt.Name = "ButtonDecrypt";
            this.ButtonDecrypt.Size = new System.Drawing.Size(141, 36);
            this.ButtonDecrypt.TabIndex = 1;
            this.ButtonDecrypt.Text = "Decrypt";
            this.ButtonDecrypt.UseVisualStyleBackColor = true;
            this.ButtonDecrypt.Click += new System.EventHandler(this.ButtonDecrypt_Click);
            // 
            // ButtonEncrypt
            // 
            this.ButtonEncrypt.Enabled = false;
            this.ButtonEncrypt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonEncrypt.Location = new System.Drawing.Point(391, 12);
            this.ButtonEncrypt.Name = "ButtonEncrypt";
            this.ButtonEncrypt.Size = new System.Drawing.Size(141, 36);
            this.ButtonEncrypt.TabIndex = 2;
            this.ButtonEncrypt.Text = "Encrypt";
            this.ButtonEncrypt.UseVisualStyleBackColor = true;
            this.ButtonEncrypt.Click += new System.EventHandler(this.ButtonEncrypt_Click);
            // 
            // ButtonAuthorize
            // 
            this.ButtonAuthorize.Location = new System.Drawing.Point(391, 96);
            this.ButtonAuthorize.Name = "ButtonAuthorize";
            this.ButtonAuthorize.Size = new System.Drawing.Size(141, 36);
            this.ButtonAuthorize.TabIndex = 0;
            this.ButtonAuthorize.Text = "Authorize";
            this.ButtonAuthorize.UseVisualStyleBackColor = true;
            this.ButtonAuthorize.Click += new System.EventHandler(this.ButtonAuthorize_Click);
            // 
            // ButtonResetForm
            // 
            this.ButtonResetForm.Enabled = false;
            this.ButtonResetForm.Location = new System.Drawing.Point(391, 138);
            this.ButtonResetForm.Name = "ButtonResetForm";
            this.ButtonResetForm.Size = new System.Drawing.Size(141, 36);
            this.ButtonResetForm.TabIndex = 5;
            this.ButtonResetForm.Text = "Reset";
            this.ButtonResetForm.UseVisualStyleBackColor = true;
            this.ButtonResetForm.Click += new System.EventHandler(this.ButtonResetForm_Click);
            // 
            // LabelVersionAuthor
            // 
            this.LabelVersionAuthor.AutoSize = true;
            this.LabelVersionAuthor.Location = new System.Drawing.Point(504, 355);
            this.LabelVersionAuthor.Name = "LabelVersionAuthor";
            this.LabelVersionAuthor.Size = new System.Drawing.Size(28, 13);
            this.LabelVersionAuthor.TabIndex = 6;
            this.LabelVersionAuthor.Text = "v1.2";
            this.LabelVersionAuthor.DoubleClick += new System.EventHandler(this.LabelVersionAuthor_DoubleClick);
            // 
            // Main_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 377);
            this.Controls.Add(this.LabelVersionAuthor);
            this.Controls.Add(this.ButtonResetForm);
            this.Controls.Add(this.ButtonAuthorize);
            this.Controls.Add(this.ButtonEncrypt);
            this.Controls.Add(this.ButtonDecrypt);
            this.Controls.Add(this.textBoxOutput);
            this.Controls.Add(this.textBoxSource);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Main_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mosaiq Encrypter \\ Decrypter";
            this.Load += new System.EventHandler(this.Main_Form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelVersionAuthor;
        private System.Windows.Forms.TextBox textBoxSource;
        private System.Windows.Forms.TextBox textBoxOutput;
        private System.Windows.Forms.Button ButtonDecrypt;
        private System.Windows.Forms.Button ButtonEncrypt;
        private System.Windows.Forms.Button ButtonAuthorize;
        private System.Windows.Forms.Button ButtonResetForm;

    }
}

