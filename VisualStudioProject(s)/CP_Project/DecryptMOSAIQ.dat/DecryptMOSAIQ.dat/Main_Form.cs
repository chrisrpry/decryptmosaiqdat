﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace DecryptMOSAIQ.dat
{
    public partial class Main_Form : Form
    {
        public Main_Form()
        {
            InitializeComponent();
        }


        //Clear the textbox when mouse clicks or keyboard focus
        private void textBoxSource_Enter(object sender, EventArgs e)
        {
            textBoxSource.Text = "";
        }
        //Encrypt Button
        private void ButtonEncrypt_Click(object sender, EventArgs e)
        {
            string pv_sData = textBoxSource.Text;
            string GrabKey = new BasicEncryption().SKOut();
            string Encrypted = new BasicEncryption().EncryptData(pv_sData, GrabKey);
            textBoxOutput.Text = Encrypted;
        }

        //Decrypt Button
        private void ButtonDecrypt_Click(object sender, EventArgs e)
        {
            string pv_sData = textBoxSource.Text;
            string GrabKey = new BasicEncryption().SKOut();
            string Decrypted = new BasicEncryption().DecryptData(pv_sData, GrabKey);
            textBoxOutput.Text = Decrypted;
        }
        //Authorize Button
        private void ButtonAuthorize_Click(object sender, EventArgs e)
        {
            Password_Form frm = new Password_Form();


            if (ButtonAuthorize.Text == "Authorize")
            {//Button is standard, so continue as normal
                frm.ShowDialog();//modal window,stops access to parent window until closed
                if (ShowTrue.GlobalValue == 1)
                {//changes disabled objects to enabled
                    EnableGroupBox();
                }//now change button label name
                ButtonAuthorize.Text = "Lock";
            }
            else if (ButtonAuthorize.Text == "Lock")
            {//this runs when the button label name is not standard and resets the form
                //ButtonAuthorize.Enabled = true;
                ShowTrue.GlobalValue = 0;
                FormClosed += (o, a) => new Main_Form().ShowDialog();
                Hide();
                Close();
            }
        }

        //Check for Drag and Drop Fuctionality - show cursor
        private void textBoxSource_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }
        //grab data from within dropped file
        private void textBoxSource_DragDrop(object sender, DragEventArgs e)
        {
            //clear text box on drop
            textBoxSource.Text = "";

            if (e.Data.GetDataPresent(DataFormats.FileDrop, true))
            {
                var files = (string[])e.Data.GetData(DataFormats.FileDrop);

                foreach (var file in files)
                {// check file extension is .dat
                    if (System.IO.Path.GetExtension(file).Equals(".dat", StringComparison.InvariantCultureIgnoreCase))
                    { //file has correct extension, take contents as string
                        string contents = System.IO.File.ReadAllText(file);
                        //send string to textbox
                        try
                        {
                            textBoxSource.AppendText(contents);
                        }
                        catch (Exception ex)
                        {
                            textBoxSource.Text = "Failed to load file, see error log below!";
                            textBoxOutput.Text = ex.Message;
                        }
                    }
                    else
                    { //file doesn't have correct extension
                        textBoxSource.Text = "ERROR: Please use *.dat file only!";
                    }

                }
            }
        }

        private void ButtonResetForm_Click(object sender, EventArgs e)
        {//reset to clear both text boxes
            textBoxSource.Text = "";
            textBoxOutput.Text = "";
        }

        public void EnableGroupBox()
        { // function to enable items
            textBoxSource.Enabled = true;
            textBoxOutput.Enabled = true;
            ButtonEncrypt.Enabled = true;
            ButtonDecrypt.Enabled = true;
            ButtonResetForm.Enabled = true;
            //
        }

        private void Main_Form_Load(object sender, EventArgs e)
        {
            //PreAuth
            if (ShowTrue.GlobalValue == 1)
            {
                EnableGroupBox();
                ButtonAuthorize.Text = "Lock";
            }
            else
            {
                string pv_sData = (Environment.UserDomainName);
                string GrabKey = new BasicEncryption().SKOut();
                string DmnEncrypted = new BasicEncryption().EncryptData(pv_sData, GrabKey);
                string Dmn = new DmnAuth().DmnAuthKey();
                if (Dmn != DmnEncrypted)
                {//Show Message and Exit!
                    MessageBox.Show("You are not authorized to run this application", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                }
            }
        }

        private void LabelVersionAuthor_DoubleClick(object sender, EventArgs e)
        {
            MessageBox.Show("Created by Chris Perry");
        }
    }
}
