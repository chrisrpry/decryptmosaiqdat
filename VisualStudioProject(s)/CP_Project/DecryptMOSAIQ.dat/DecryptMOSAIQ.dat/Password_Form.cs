﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DecryptMOSAIQ.dat
{
    public partial class Password_Form : Form
    {
        public Password_Form()
        {
            InitializeComponent();
        }
        //Logic for Go Button!
        private void ButtonGo_Click(object sender, EventArgs e)
        {//compare text input with stored encrypted password
            string pv_sData = textBoxPassword.Text;
            string GrabKey = new BasicEncryption().SKOut();
            string pwdEncrypted = new BasicEncryption().EncryptData(pv_sData, GrabKey);
            string pwd = new PwdAuth().pwd();


            if (pwd == pwdEncrypted)
            {//successful authentication, close password window un-hide all.
                Main_Form frm = new Main_Form();
                SetShowTrue();
                this.Close();
            }
            else
            {//Wrong Password
                MessageBox.Show("Wrong Password!","Error!",MessageBoxButtons.OK,MessageBoxIcon.Stop);
            }
        }
        static void SetShowTrue()
        {
            // Set global integer.
            ShowTrue.GlobalValue = 1;
        }

    }
}
