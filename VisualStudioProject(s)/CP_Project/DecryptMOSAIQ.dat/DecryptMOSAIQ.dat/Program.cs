﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace DecryptMOSAIQ.dat
{
    static class Program //changed to public statis class
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static Main_Form Mainform;
        [STAThread]
        static void Main(string[] args) //added 'string[] args' to Main() and changed to public static void
        {
            //Set Form as a variable (so can call functions within).
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var MainForm = new Main_Form();
            /// Application.Run(new Main_Form()); //commented out as now using variable.

            //Check for switches/arguments passed from the command line.
            foreach (string switches in args) //check for args/switches on startup
            {
                if (switches.StartsWith("/"))
                {
                    switch (switches.Substring(1)) //'switch' statement os like a multiple set of if statements.
                    {
                        case "OIS": //if switch is /OIS the do the following:
                            ShowTrue.GlobalValue = 1;
                            break;
                        default: //if no switch(es)
                            break;
                    }
                }

            }
            Application.Run(MainForm);
        }
    }
}

//Encryption\Decryption
public class BasicEncryption
{
    public string DecryptData(string pv_sData, string pv_sSecondaryKey)
    {//Decryption Method
        TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider = new TripleDESCryptoServiceProvider();
        tripleDESCryptoServiceProvider.IV = new byte[8];
        PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(pv_sSecondaryKey, new byte[0]);
        tripleDESCryptoServiceProvider.Key = passwordDeriveBytes.CryptDeriveKey("RC2", "MD5", 128, new byte[8]);
        byte[] array = new byte[]
			{
			32
			}

        ;
        try
        {
            array = Convert.FromBase64String(pv_sData);
        }
        catch (FormatException)
        {
        }
        MemoryStream memoryStream = new MemoryStream(pv_sData.Length);
        CryptoStream cryptoStream = new CryptoStream(memoryStream, tripleDESCryptoServiceProvider.CreateDecryptor(), CryptoStreamMode.Write);
        cryptoStream.Write(array, 0, array.Length);
        try
        {
            cryptoStream.FlushFinalBlock();
        }
        catch (System.Security.Cryptography.CryptographicException)
        {
            return "Error: Either you have not entered text or the data is corrupt";
        }
        byte[] array2 = new byte[memoryStream.Length];
        memoryStream.Position = 0L;
        memoryStream.Read(array2, 0, (int)memoryStream.Length);
        cryptoStream.Close();

        return Encoding.UTF8.GetString(array2);
    }


    public string EncryptData(string pv_sData, string pv_sSecondaryKey)
    {//Encryption Method
        TripleDESCryptoServiceProvider cryptoServiceProvider = new TripleDESCryptoServiceProvider();
        cryptoServiceProvider.IV = new byte[8];
        PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(pv_sSecondaryKey, new byte[0]);
        cryptoServiceProvider.Key = passwordDeriveBytes.CryptDeriveKey("RC2", "MD5", 128, new byte[8]);
        MemoryStream memoryStream = new MemoryStream(pv_sData.Length * 2);
        CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, cryptoServiceProvider.CreateEncryptor(), CryptoStreamMode.Write);
        byte[] bytes = Encoding.UTF8.GetBytes(pv_sData);
        cryptoStream.Write(bytes, 0, bytes.Length);
        cryptoStream.FlushFinalBlock();
        byte[] numArray = new byte[memoryStream.Length];
        memoryStream.Position = 0L;
        memoryStream.Read(numArray, 0, (int)memoryStream.Length);
        cryptoStream.Close();
        return Convert.ToBase64String(numArray);
    }


    public string SKOut()
    {//SecondaryKey
        char[] array = "EF637E2D5FB2-5AED-B74C-9244-D340FC8C".ToCharArray();
        Array.Reverse(array);
        string ReverseArray = new string(array);
        string pv_sSecondaryKey = ReverseArray.Replace("-", "*IMPACELEKTA*");
        return (pv_sSecondaryKey);
    }
}
//Authentication Methods
public class PwdAuth
{//set the password to use the application
    public string pwd()
    {
        string pwd = "qUU+r+GvRGw=";
        return (pwd);
    }

}
public class DmnAuth
{//Set Primary Authentication
    public string DmnAuthKey()
    {
        string DmnAuthKey = "gzZDwmplosY=";
        return (DmnAuthKey);
    }
}
//Show/Hide Objects
public static class ShowTrue
{//make a global int editable from any part of project
    public const int GlobalInt = 0;
    static int _globalValue;
    public static int GlobalValue
    {
        get
        {
            return _globalValue;
        }
        set
        {
            _globalValue = value;
        }
    }
    public static bool GlobalBoolean;
}