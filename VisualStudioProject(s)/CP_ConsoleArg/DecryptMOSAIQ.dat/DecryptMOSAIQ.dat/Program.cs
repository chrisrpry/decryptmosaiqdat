﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Threading.Tasks;

namespace DecryptMOSAIQ.dat
{
    class Program
    {
        static void Main(string[] args)
        {
            string mosaiqdatpath = args[0];
            if (File.Exists(mosaiqdatpath))
            {
                string mosaiqdatcontents = File.ReadAllText(mosaiqdatpath);
                string pv_sData = mosaiqdatcontents;
                string GrabKey = new BasicEncryption().SKOut();
                string Decrypted = new BasicEncryption().DecryptData(pv_sData, GrabKey);
                Console.WriteLine(Decrypted);
            }
        }
        public class BasicEncryption
        {
            public string DecryptData(string pv_sData, string pv_sSecondaryKey)
            {//Decryption Method
                TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider = new TripleDESCryptoServiceProvider();
                tripleDESCryptoServiceProvider.IV = new byte[8];
                PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(pv_sSecondaryKey, new byte[0]);
                tripleDESCryptoServiceProvider.Key = passwordDeriveBytes.CryptDeriveKey("RC2", "MD5", 128, new byte[8]);
                byte[] array = new byte[]
                    {
            32
                    }

                ;
                try
                {
                    array = Convert.FromBase64String(pv_sData);
                }
                catch (FormatException)
                {
                }
                MemoryStream memoryStream = new MemoryStream(pv_sData.Length);
                CryptoStream cryptoStream = new CryptoStream(memoryStream, tripleDESCryptoServiceProvider.CreateDecryptor(), CryptoStreamMode.Write);
                cryptoStream.Write(array, 0, array.Length);
                try
                {
                    cryptoStream.FlushFinalBlock();
                }
                catch (System.Security.Cryptography.CryptographicException)
                {
                    return "Error: Either you have not entered text or the data is corrupt";
                }
                byte[] array2 = new byte[memoryStream.Length];
                memoryStream.Position = 0L;
                memoryStream.Read(array2, 0, (int)memoryStream.Length);
                cryptoStream.Close();

                return Encoding.UTF8.GetString(array2);
            }

            public string SKOut()
            {//SecondaryKey
                char[] array = "EF637E2D5FB2-5AED-B74C-9244-D340FC8C".ToCharArray();
                Array.Reverse(array);
                string ReverseArray = new string(array);
                string pv_sSecondaryKey = ReverseArray.Replace("-", "*IMPACELEKTA*");
                return (pv_sSecondaryKey);
            }
        }
    }
}
