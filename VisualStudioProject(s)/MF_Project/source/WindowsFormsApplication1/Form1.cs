using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
namespace WindowsFormsApplication1
{
	public class Form1 : Form
	{
		private IContainer components;
		private Label label1;
		private TextBox textBox1;
		private Button button1;
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}
		private void InitializeComponent()
		{
			this.label1 = new Label();
			this.textBox1 = new TextBox();
			this.button1 = new Button();
			base.SuspendLayout();
			this.label1.AutoSize = true;
			this.label1.Location = new Point(23, 23);
			this.label1.Name = "label1";
			this.label1.Size = new Size(53, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Password";
			this.textBox1.Location = new Point(101, 23);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Size(100, 20);
			this.textBox1.TabIndex = 1;
			this.textBox1.UseSystemPasswordChar = true;
			this.button1.Location = new Point(140, 57);
			this.button1.Name = "button1";
			this.button1.Size = new Size(66, 30);
			this.button1.TabIndex = 2;
			this.button1.Text = "Login";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new EventHandler(this.button1_Click);
			base.AutoScaleDimensions = new SizeF(6f, 13f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(217, 95);
			base.Controls.Add(this.button1);
			base.Controls.Add(this.textBox1);
			base.Controls.Add(this.label1);
			base.Name = "Form1";
			this.Text = "MQ Decryptor";
			base.ResumeLayout(false);
			base.PerformLayout();
		}
		public Form1()
		{
			this.InitializeComponent();
		}
		public void StartDecryption()
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();
			DialogResult dialogResult = openFileDialog.ShowDialog();
			bool flag;
			string pv_sData;
			if (dialogResult == DialogResult.Cancel)
			{
				flag = true;
			}
			else
			{
				flag = false;
				pv_sData = "emptystring";
			}
			if (dialogResult == DialogResult.OK)
			{
				string fileName = openFileDialog.FileName;
				try
				{
					using (StreamReader streamReader = new StreamReader(fileName))
					{
						pv_sData = streamReader.ReadToEnd();
					}
					goto IL_5E;
				}
				catch (IOException)
				{
					flag = false;
					pv_sData = "lala";
					goto IL_5E;
				}
			}
			flag = true;
			pv_sData = "wrongstring";
			IL_5E:
			if (flag)
			{
				Application.Exit();
				Environment.Exit(1);
			}
			char[] array = "EF637E2D5FB2-5AED-B74C-9244-D340FC8C".ToCharArray();
			Array.Reverse(array);
			string text = new string(array);
			string pv_sSecondaryKey = text.Replace("-", "*IMPACELEKTA*");
			string text2 = new BasicEncryption().DecryptData(pv_sData, pv_sSecondaryKey);
			MessageBox.Show(text2);
		}
		private void button1_Click(object sender, EventArgs e)
		{
			if (this.textBox1.Text.ToString().CompareTo("CryptoMagic2014") == 0)
			{
				this.StartDecryption();
				return;
			}
			MessageBox.Show("that was incorrect");
		}
	}
}
