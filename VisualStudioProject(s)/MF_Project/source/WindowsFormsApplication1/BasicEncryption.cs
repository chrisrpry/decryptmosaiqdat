using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
namespace WindowsFormsApplication1
{
	public class BasicEncryption
	{
		public string EncryptData(string pv_sData, string pv_sSecondaryKey)
		{
			TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider = new TripleDESCryptoServiceProvider();
			tripleDESCryptoServiceProvider.IV = new byte[8];
			PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(pv_sSecondaryKey, new byte[0]);
			tripleDESCryptoServiceProvider.Key = passwordDeriveBytes.CryptDeriveKey("RC2", "MD5", 128, new byte[8]);
			MemoryStream memoryStream = new MemoryStream(pv_sData.Length * 2);
			CryptoStream cryptoStream = new CryptoStream(memoryStream, tripleDESCryptoServiceProvider.CreateEncryptor(), CryptoStreamMode.Write);
			byte[] bytes = Encoding.UTF8.GetBytes(pv_sData);
			cryptoStream.Write(bytes, 0, bytes.Length);
			cryptoStream.FlushFinalBlock();
			byte[] array = new byte[memoryStream.Length];
			memoryStream.Position = 0L;
			memoryStream.Read(array, 0, (int)memoryStream.Length);
			cryptoStream.Close();
			return Convert.ToBase64String(array);
		}
		public string DecryptData(string pv_sData, string pv_sSecondaryKey)
		{
			TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider = new TripleDESCryptoServiceProvider();
			tripleDESCryptoServiceProvider.IV = new byte[8];
			PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(pv_sSecondaryKey, new byte[0]);
			tripleDESCryptoServiceProvider.Key = passwordDeriveBytes.CryptDeriveKey("RC2", "MD5", 128, new byte[8]);
			byte[] array = new byte[]
			{
				32
			};
			try
			{
				array = Convert.FromBase64String(pv_sData);
			}
			catch (FormatException)
			{
			}
			MemoryStream memoryStream = new MemoryStream(pv_sData.Length);
			CryptoStream cryptoStream = new CryptoStream(memoryStream, tripleDESCryptoServiceProvider.CreateDecryptor(), CryptoStreamMode.Write);
			cryptoStream.Write(array, 0, array.Length);
			cryptoStream.FlushFinalBlock();
			byte[] array2 = new byte[memoryStream.Length];
			memoryStream.Position = 0L;
			memoryStream.Read(array2, 0, (int)memoryStream.Length);
			cryptoStream.Close();
			return Encoding.UTF8.GetString(array2);
		}
	}
}
