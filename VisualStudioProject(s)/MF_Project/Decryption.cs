using System;
using System.Security.Cryptography;
using System.IO;
using System.Text;

public class Program
{
	public class BasicEncryption
	{
		public string DecryptData(string pv_sData, string pv_sSecondaryKey)
		{
			TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider = new TripleDESCryptoServiceProvider();
			tripleDESCryptoServiceProvider.IV = new byte[8];
			PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(pv_sSecondaryKey, new byte[0]);
			tripleDESCryptoServiceProvider.Key = passwordDeriveBytes.CryptDeriveKey("RC2", "MD5", 128, new byte[8]);
			byte[] array = new byte[]
			{
			32
			}

			;
			try
			{
				array = Convert.FromBase64String(pv_sData);
			}
			catch (FormatException)
			{
			}
			MemoryStream memoryStream = new MemoryStream(pv_sData.Length);
			CryptoStream cryptoStream = new CryptoStream(memoryStream, tripleDESCryptoServiceProvider.CreateDecryptor(), CryptoStreamMode.Write);
			cryptoStream.Write(array, 0, array.Length);
			cryptoStream.FlushFinalBlock();
			byte[] array2 = new byte[memoryStream.Length];
			memoryStream.Position = 0L;
			memoryStream.Read(array2, 0, (int)memoryStream.Length);
			cryptoStream.Close();

			return Encoding.UTF8.GetString(array2);
		}
	}

	public static void Main()
	{
		char[] array = "EF637E2D5FB2-5AED-B74C-9244-D340FC8C".ToCharArray();
		Array.Reverse(array);
		string text = new string (array);
		Console.WriteLine("Private Key (=MQ APP GUID reversed with '-' replaced with '~IMPACELEKTA'):");
		string pv_sSecondaryKey = text.Replace("-", "*IMPACELEKTA*");
		Console.WriteLine(pv_sSecondaryKey);
		Console.WriteLine("Encrypted Data ()inside of mosaiq.dat:");
		string pv_sData = "w12FbAY+eW60W4WSf3U8Wzd3mVrOXtLGbtHdxGXqQXCH9L14xZMaRJ3b20otAOfFPGaZ4ti+OVX1fO1n1i2LxkTS2VamhWw0tFA0gbdGJDw1SQ5GhK/Oa92n/KwySHln8N0QAaKtp1rSPy3r0BRoufdVIh0aaIZ2DWQjkMzFCSZMTeB612XmKfTAWfLxeypaHZ1duiiUOUASjUHPZZpXhijBJhHNXCcSN/O1NMUN6y/2+6gAHkmys9cZG3i2RWPDCtqChc5t7VruIcu+yHgmyxKYQXxIR99isJcw6IntapDuctS7vA2WwV/bkHVO6bAmJffgATiujrmw5O7+JPJ/FNhY1UWiqA+JgJW8yy8sguj0CCwfcvu6hf2ncqAFqjbwuyy0ZeLpYGJFLXsc0qNJ0OhaIgvUUFytb/ZuGNQvZRuLZ7sTg713W/bhYoPld65eXfaLSFQ+DOQ88LoYepF7hShugeeBNiXidDQuznMmvS3v9dpXWTaYtXi+1Y2WUo0HkTp3RzFL+LW4YuWMluSwUep6gDfo4DMhRN9Zu8SVzrgj5KB7V1ykdS86BrvJEGZt95l3ZtsLmL8FMJuw2ZMxecZdtD+c2nqj0goS/AS1SoulLFkSx9d18KvJx2OdfW7rORCOP8SrgwH2PI0ctcPHtQ==";
		Console.WriteLine(pv_sData);
		string text2 = new BasicEncryption().DecryptData(pv_sData, pv_sSecondaryKey);
		Console.WriteLine("Decrypted Data (standard XML file) :");
		Console.WriteLine(text2);
	}
}